<?php

require 'AWSSDK/aws.phar';

$az = file_get_contents('http://169.254.169.254/latest/meta-data/placement/availability-zone');
$region = substr($az, 0, -1);

$ssm_client = new Aws\Ssm\SsmClient([
    'version' => 'latest',
    'region'  => $region
]);

$result = $ssm_client->GetParametersByPath(['Path' => '/sdn/prod']);

$showServerBool = "";
$db_masterurl = "";
$db_rrurl = "";
$db_name = "";
$db_user = "";
$db_password = "";
$domain = "";
$s3URL = "";

foreach($result['Parameters'] as $p) {
    if ($p['Name'] == '/sdn/prod/domain') $domain = $p['Value'];
    if ($p['Name'] == '/sdn/prod/s3URL') $s3URL = $p['Value'];
    if ($p['Name'] == '/sdn/prod/showServerBool') $showServerBool = $p['Value'];
    if ($p['Name'] == '/sdn/prod/dbMasterUrl') $db_masterurl = $p['Value'];
    if ($p['Name'] == '/sdn/prod/dbRRUrl') $db_rrurl = $p['Value'];
    if ($p['Name'] == '/sdn/prod/dbName') $db_name = $p['Value'];
    if ($p['Name'] == '/sdn/prod/dbUser') $db_user = $p['Value'];
    if ($p['Name'] == '/sdn/prod/dbPassword') $db_password = $p['Value'];
}

?>